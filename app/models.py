from app import db


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(50), index=True)
    last_name = db.Column(db.String(50), index=True)
    program = db.Column(db.String(20), index=True)
    email = db.Column(db.String(120), index=True, unique=True)

    def __repr__(self):
        return "<User {}>".format(self.username)


class Review(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    course = db.Column(db.String(10), index=True)
    developer = db.Column(db.String(10), index=True)
    program = db.Column(db.String(20), index=True)
    stage = db.Column(db.String(15), index=True)

    def __repr__(self):
        return f"<Review {self.course}/{self.id}>"
