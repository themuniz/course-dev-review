from flask import render_template
from app import app
from app.models import Review

@app.route("/")
@app.route("/index")
def index():
    user = {"first_name": "Antonia"}
    reviews = Review.query.all()
    return render_template("index.html", user=user, reviews=reviews)
